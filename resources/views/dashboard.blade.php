@extends('layouts.main')

@section('content')
<h2 class="page-header">Products</h2>
<table class="table table-bordered table-responsive table-hovered" id="dataTable">
    <thead>
        <tr>
            <th class="heading-row">ID</th>
            <th class="heading-row">Name</th>
            <th class="heading-row">Price</th>
            <th class="heading-row">Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr class="product-row">
            <td>{{ $product->id }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->priceFormatted }}</td>
            <td>
                <a href="{{ route('api.product.buy', [$product->id]) }}" class="btn btn-sm btn-primary product-buy product-buy-{{ $product->id }}">Buy</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{!! $products->links() !!}
@stop

@section('scripts')
<script src="{{ asset('vendor/jquery.tablesorter.min.js') }}"></script>
<script>
!function($){
    $("#dataTable").tablesorter();

    $(".product-buy").click(function(e){
        e.preventDefault();
        var row = $(this).closest('.product-row');
        $.ajax({
            url: $(this).attr('href'),
            type: 'post',
            success: function(){
                row.hide();
            },
            error: function(){
                alert('Error happen!');
            }
        })
    });
}(jQuery);
</script>
@stop