## Installation

To install this application you have to:
 - install php 5.6+ version
 - clone this repository
 - create database (for example _products_app_)
 - copy .env.example file to .env file and setup your settings
 - install composer dependencies:
```bash
php composer.phar install
```
 - run migrations with seeds:
```bash
php artisan migrate --seed
``` 
 - run php server
```bash
php artisan serve
```
 - open [http://localhost:8000/](http://localhost:8000/) in your browser
 
 If you need to refresh your migrations with seeds run:
```bash
php artisan migrate:refresh --seed
```


## Testing

To run phpunit tests you have to create database for testing with name _test_products_app_
Run tests:
```bash
php phpunit.phar
```
Every test use testing database and refresh migrations every time

## REST API Examples

##### Creating products:

```bash
POST /api/product
name=test+product&price=1000
```
* name, price - required parameters

##### Creating vouchers:
```
POST /api/voucher
start_date=2017-01-01&end_date=2017-12-31&discount=10
```
* discount - required parameters
* start_date,end_date - non required parameters in date format (Y-m-d)

##### To add a voucher bind to a certain product
```
POST /api/product/PRODUCT_ID/vouchers/VOUCHER_ID
```
* PRODUCT_ID, VOUCHER_ID - valid product and voucher ids

##### To remove voucher bind from a certain product
```
DELETE /api/product/PRODUCT_ID/vouchers/VOUCHER_ID
```
* PRODUCT_ID, VOUCHER_ID - valid product and voucher ids

##### To buy a product
```
POST /api/product/buy/PRODUCT_ID
```
* PRODUCT_ID - valid product id

## Other

For table sorting I've used jquery library jquery.tablesorter

##### Discount calculator
 For calculating discounts for products I wrote _SimpleDiscountCalculator_ class that 
 binding to application as _ProductPriceCalculator_ with Dependency Injection in _AppServiceProvider_.
 It makes discount calculating very flexible, we can wrote more discount calculators with 
 different and complicated logic (for example fixed discount) and redefine it in one place.
 Also this calculator is product-specific. We can define new product types with inherited classes
 and redefine _getPriceCalculator()_ method to make discounts based on product type, parameters or 
 even disable discounts for specific products. It's all easy