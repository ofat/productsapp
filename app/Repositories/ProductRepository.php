<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{

    /**
     * @var int
     */
    protected $perPage = 15;

    /**
     * @var Product
     */
    protected $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|Product[]
     */
    public function getAvailable()
    {
        return $this->model
            ->where('available', Product::AVAILABLE)
            ->paginate($this->perPage);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|Product
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param int $perPage
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;
    }
}