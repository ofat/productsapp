<?php

namespace App\Repositories;

use App\Models\Discount;

/**
 * Class DiscountRepository
 * @package App\Repositories
 */
class DiscountRepository
{
    /**
     * @var Discount
     */
    protected $model;

    public function __construct(Discount $model)
    {
        $this->model = $model;
    }

    /**
     * @param $value
     * @return \Illuminate\Database\Eloquent\Model|Discount
     */
    public function getByValue($value)
    {
        return $this->model
            ->where('value', $value)
            ->firstOrFail();
    }
}