<?php

namespace App\PriceCalculator;

/**
 * Interface Calculable
 * @package App\PriceCalculator
 */
interface Calculable
{
    /**
     * Calculate product price with vouchers' discounts
     * @return float
     */
    public function calculate();
}