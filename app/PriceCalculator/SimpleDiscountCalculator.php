<?php

namespace App\PriceCalculator;

/**
 * Class SimpleDiscountCalculator
 * @package App\PriceCalculator
 */
class SimpleDiscountCalculator extends BasePriceCalculator
{
    /**
     * Max discount per product
     */
    const MAX_DISCOUNT = 60;

    /**
     * @var int
     */
    protected $totalDiscount = 0;

    /**
     * @inheritdoc
     */
    public function calculate()
    {
        foreach($this->vouchers as $voucher)
        {
            $this->totalDiscount += $voucher->discount->value;
        }

        $this->checkMaxDiscount();

        $price = $this->rawPrice * (100 - $this->totalDiscount) / 100;
        return $price;
    }

    /**
     * Check if current total discount more than max discount
     */
    protected function checkMaxDiscount()
    {
        if($this->totalDiscount > static::MAX_DISCOUNT)
        {
            $this->totalDiscount = static::MAX_DISCOUNT;
        }
    }
}