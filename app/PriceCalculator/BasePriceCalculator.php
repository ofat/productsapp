<?php

namespace App\PriceCalculator;

use App\Models\Voucher;

/**
 * Class BasePriceCalculator
 * @package App\PriceCalculator
 */
abstract class BasePriceCalculator implements Calculable
{
    /**
     * @var Voucher[]
     */
    protected $vouchers = [];

    /**
     * @var float
     */
    protected $rawPrice;

    /**
     * @param Voucher[] $vouchers
     * @return BasePriceCalculator
     */
    public function setVouchers($vouchers)
    {
        $this->vouchers = $vouchers;
        return $this;
    }

    /**
     * @param float $rawPrice
     * @return BasePriceCalculator
     */
    public function setRawPrice($rawPrice)
    {
        $this->rawPrice = $rawPrice;
        return $this;
    }
}