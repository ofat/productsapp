<?php

namespace App\Models;

use App\PriceCalculator\BasePriceCalculator;
use App\PriceCalculator\Calculable;
use App\PriceCalculator\SimpleDiscountCalculator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models
 * @mixin \Eloquent
 *
 * @property $id
 * @property $name
 * @property double $price
 * @property int $available
 * @property Voucher[] $vouchers
 * @property Voucher[] $activeVouchers
 *
 */
class Product extends Model
{
    protected $fillable = [
        'name',
        'price',
        'available'
    ];

    public $timestamps = false;

    const AVAILABLE = 1;
    const NOT_AVAILABLE = 0;

    public $with = ['vouchers'];
    public $appends = ['vouchers'];

    /**
     * All product vouchers
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function vouchers()
    {
        return $this->belongsToMany(Voucher::class, 'product_vouchers');
    }

    /**
     * Currently available vouchers for product
     * @return mixed
     */
    public function activeVouchers()
    {
        return $this->vouchers()->active();
    }

    /**
     * Defined with dependency injection in \App\Providers\AppServiceProvider
     * @see \App\Providers\AppServiceProvider
     * @return BasePriceCalculator
     */
    protected function getPriceCalculator()
    {
        return \App::make('ProductPriceCalculator');
    }

    /**
     * Calculate product price with current discount calculator
     * @return float
     */
    public function getPrice()
    {
        $calculator = $this->getPriceCalculator();
        $calculator
            ->setVouchers($this->activeVouchers)
            ->setRawPrice($this->price);

        $price = $calculator->calculate();
        return $price;
    }

    /**
     * Format price number
     * @return string
     */
    public function getPriceFormattedAttribute()
    {
        return number_format($this->getPrice(), 0, '.', ' ');
    }

    public function getVouchersAttribute()
    {
        return $this->vouchers()->get();
    }
}
