<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Discount
 * @package App\Models
 * @mixin \Eloquent
 *
 * @property int $value
 */
class Discount extends Model
{
    protected $fillable = [
        'value'
    ];

    public $timestamps = false;
}
