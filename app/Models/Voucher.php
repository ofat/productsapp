<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Voucher
 * @package App\Models
 * @mixin \Eloquent
 *
 * @property int $id
 * @property $start_date
 * @property $end_date
 * @property int $discount_id
 * @property Discount $discount
 */
class Voucher extends Model
{
    protected $fillable = [
        'start_date',
        'end_date',
        'discount_id'
    ];

    public $timestamps = false;
    public $hidden = ['discount', 'pivot'];
    public $appends = ['discount_value'];

    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }

    public function scopeActive(Builder $builder)
    {
        return $builder
            ->where(function(Builder $query){
                $query->where('start_date', '<=', \DB::raw('now()'))
                    ->orWhereNull('start_date');
            })
            ->where(function(Builder $query){
                $query->where('end_date', '>=', \DB::raw('now()'))
                    ->orWhere('end_date');
            });
    }

    public function getDiscountValueAttribute()
    {
        if(isset($this->discount))
            return $this->discount->value;

        return $this->discount()->firstOrFail()->value;
    }
}
