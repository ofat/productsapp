<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductVoucher
 * @package App\Models
 *
 * @property int $product_id
 * @property int $voucher_id
 */
class ProductVoucher extends Model
{
    protected $fillable = [
        'product_id',
        'voucher_id'
    ];

    public $timestamps = false;
}
