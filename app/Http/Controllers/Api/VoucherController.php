<?php

namespace App\Http\Controllers\Api;

use App\Models\Discount;
use App\Models\Voucher;
use App\Repositories\DiscountRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class VoucherController extends BaseApiController
{
    /**
     * @var Voucher
     */
    protected $model;

    /**
     * @var DiscountRepository
     */
    protected $discountRepository;

    public function __construct(Voucher $model, DiscountRepository $discountRepository)
    {
        $this->model = $model;
        $this->discountRepository = $discountRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'date',
            'end_date' => 'date',
            'discount' => 'required|exists:discounts,value'
        ]);

        $discount = $this->discountRepository->getByValue($request->get('discount'));
        $attributes = $request->only(['start_date', 'end_date']);
        $attributes['discount_id'] = $discount->id;

        $voucher = $this->model->create($attributes);

        return response()->json($voucher);
    }

}
