<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends BaseApiController
{
    /**
     * @var Product
     */
    protected $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'price' => 'required|numeric'
        ]);

        $product = $this->model->create($request->all());

        return response()->json($product);
    }

    public function buy(Product $product)
    {
        $product->available = Product::NOT_AVAILABLE;
        $product->save();

        return response()->json($product);
    }
}
