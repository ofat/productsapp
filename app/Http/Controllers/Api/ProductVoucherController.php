<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Models\Voucher;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

/**
 * Class ProductVoucherController
 * @package App\Http\Controllers\Api
 */
class ProductVoucherController extends BaseApiController
{

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function store(Request $request, Product $product, Voucher $voucher)
    {
        /**
         * Avoid of adding multiple same vouchers to a product
         */
        $currentVouchers = $product->vouchers()->pluck('voucher_id');
        $currentVouchers[] = $voucher->id;
        $product->vouchers()->sync($currentVouchers);

        $result = $this->productRepository->getById($product->id);
        return response()->json($result);
    }

    public function remove(Request $request, Product $product, Voucher $voucher)
    {
        $product->vouchers()->detach($voucher->id);

        $result = $this->productRepository->getById($product->id);
        return response()->json($result);
    }
}
