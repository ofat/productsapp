<?php

use App\Models\Product;
use App\Models\Voucher;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductVoucherControllerTest extends TestCase
{

    use DatabaseMigrations;

    public function testStore()
    {
        $this->runDatabaseMigrations();

        /**
         * @var Product $product
         */
        $product = factory(Product::class)->create();
        /**
         * @var Voucher $voucher
         */
        $voucher = factory(Voucher::class)->create();

        $url = route('api.product.vouchers.store', [$product->id, $voucher->id]);

        $this->post($url);

        $this->seeInDatabase('product_vouchers',[
            'product_id' => $product->id,
            'voucher_id' => $voucher->id
        ]);
    }

    public function testRemove()
    {
        $this->runDatabaseMigrations();

        /**
         * @var Product $product
         */
        $product = factory(Product::class)->create();
        /**
         * @var Voucher $voucher
         */
        $voucher = factory(Voucher::class)->create();

        $product->vouchers()->save($voucher);

        $this->seeInDatabase('product_vouchers',[
            'product_id' => $product->id,
            'voucher_id' => $voucher->id
        ]);

        $url = route('api.product.vouchers.remove', [$product->id, $voucher->id]);

        $this->delete($url);

        $this->dontSeeInDatabase('product_vouchers',[
            'product_id' => $product->id,
            'voucher_id' => $voucher->id
        ]);
    }
}
