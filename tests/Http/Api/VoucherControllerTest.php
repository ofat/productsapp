<?php

use App\Models\Discount;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VoucherControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testStore()
    {
        $this->runDatabaseMigrations();

        $startDate = '2017-01-01';
        $endDate = '2017-12-31';
        $url = route('api.voucher.store');
        $discount = factory(Discount::class)->create();

        $this->post($url, [
            'start_date' => $startDate,
            'end_date' => $endDate,
            'discount' => $discount->value
        ]);

        $this->seeInDatabase('vouchers', [
            'start_date' => $startDate,
            'end_date' => $endDate,
            'discount_id' => $discount->id
        ]);

        // wrong dates
        $this->post($url, [
            'start_date' => '01-01-1000',
            'end_date' => $endDate,
            'discount' => $discount->value
        ]);

        $this->dontSeeInDatabase('vouchers', [
            'start_date' => '01-01-1000',
            'end_date' => $endDate,
            'discount_id' => $discount->id
        ]);
    }
}
