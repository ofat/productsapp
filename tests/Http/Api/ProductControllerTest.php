<?php

use App\Models\Product;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testStore()
    {
        $this->runDatabaseMigrations();

        $this->post(route('api.product.store'), [
            'name' => 'Test Product',
            'price' => 1000
        ]);

        $this->seeInDatabase('products', [
            'name' => 'Test Product',
            'price' => 1000
        ]);
    }

    public function testBuy()
    {
        $this->runDatabaseMigrations();

        $product = factory(Product::class)->create();
        $url = route('api.product.buy', $product->id);

        $this->post($url);

        $this->seeInDatabase('products', [
            'id' => $product->id,
            'available' => Product::NOT_AVAILABLE
        ]);
    }
}
