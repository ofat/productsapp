<?php

use App\Models\Product;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DashboardControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testMain()
    {
        $this->runDatabaseMigrations();

        $product = factory(Product::class)->create();

        $this->visit('/')
            ->see('Products')
            ->see($product->name);
    }
}
