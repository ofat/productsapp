<?php

use App\Models\Discount;
use App\Models\Voucher;
use App\PriceCalculator\BasePriceCalculator;
use App\PriceCalculator\SimpleDiscountCalculator;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SimpleDiscountCalculatorTest extends TestCase
{

    use DatabaseMigrations;

    public function testCalculate()
    {
        $this->runDatabaseMigrations();

        // 3 vouchers with 10% discount = 30% discount
        $vouchers = factory(Voucher::class, 3)
            ->states(['active'])
            ->make();

        $calculator = (new SimpleDiscountCalculator())
            ->setVouchers($vouchers)
            ->setRawPrice(1000);

        $this->assertEquals(700, $calculator->calculate());

        // 7 vouchers with 10% discount = max 60% discount
        $vouchers = factory(Voucher::class, 7)
            ->states(['active'])
            ->make();

        $calculator = (new SimpleDiscountCalculator())
            ->setVouchers($vouchers)
            ->setRawPrice(1000);

        $this->assertEquals(400, $calculator->calculate());
    }
}
