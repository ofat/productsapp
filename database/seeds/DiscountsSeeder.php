<?php

use Illuminate\Database\Seeder;

class DiscountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('discounts')->insert([
            [
                'value' => 10
            ],
            [
                'value' => 15
            ],
            [
                'value' => 20
            ],
            [
                'value' => 25
            ]
        ]);
    }
}
