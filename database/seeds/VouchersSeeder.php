<?php

use Illuminate\Database\Seeder;

class VouchersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('vouchers')->insert([
            // non-active vouchers
            [
                'id' => 1,
                'start_date' => '2016-12-01',
                'end_date' => '2016-12-31',
                'discount_id' => 1 // 10%
            ],
            [
                'id' => 2,
                'start_date' => null,
                'end_date' => '2017-01-15',
                'discount_id' => 2 // 15%
            ],
            [
                'id' => 3,
                'start_date' => '2016-12-15',
                'end_date' => '2017-01-15',
                'discount_id' => 3 // 20%
            ],
            [
                'id' => 4,
                'start_date' => '2017-03-01',
                'end_date' => '2017-04-15',
                'discount_id' => 1 // 10%
            ],

            // active vouchers
            [
                'id' => 5,
                'start_date' => '2017-01-01',
                'end_date' => '2017-02-15',
                'discount_id' => 4 // 25%
            ],
            [
                'id' => 6,
                'start_date' => '2017-01-01',
                'end_date' => '2017-02-15',
                'discount_id' => 1 // 10%
            ],
            [
                'id' => 7,
                'start_date' => '2017-01-01',
                'end_date' => null,
                'discount_id' => 2 // 15%
            ],
            [
                'id' => 8,
                'start_date' => '2017-01-01',
                'end_date' => '2017-02-15',
                'discount_id' => 4 // 25%
            ],
            [
                'id' => 9,
                'start_date' => '2017-01-05',
                'end_date' => '2017-02-18',
                'discount_id' => 3 // 20%
            ]
        ]);
    }
}
