<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->insert([
            [
                'name' => 'MacBook',
                'price' => 2000
            ],
            [
                'name' => 'Iphone',
                'price' => 1000
            ],
            [
                'name' => 'iMac',
                'price' => 5000
            ],
            [
                'name' => 'Apple Watch',
                'price' => 500
            ],
            [
                'name' => 'iPad',
                'price' => 1000
            ]
        ]);

        /**
         * id = 1-4 non active vouchers
         * id = 5-9 active voucher
         */
        $productVouchers = [
            /**
             * product 1 - total = 25%
             */
            [1, 1], // 10%
            [1, 3], // 20%
            [1, 5], // 25%
            /**
             * product 2 - total = 70%
             */
            [2, 9], // 20%
            [2, 8], // 25%
            [2, 5], // 25%
            /**
             * product 3 - total = 25%
             */
            [3, 4], // 10%
            [3, 5], // 25%
            /**
             * product 4 - total = 15%
             */
            [4, 7], // 15%
            /**
             * product 5 without discount
             */
        ];
        $data = $this->prepareDataForInsert($productVouchers);
        \DB::table('product_vouchers')->insert($data);
    }

    /**
     * @param array $productVouchers
     * @return array
     */
    protected function prepareDataForInsert(array $productVouchers)
    {
        $data = [];
        foreach($productVouchers as $productVoucher)
        {
            list($productId, $voucherId) = $productVoucher;
            $data[] = [
                'product_id' => $productId,
                'voucher_id' => $voucherId
            ];
        }

        return $data;
    }
}
