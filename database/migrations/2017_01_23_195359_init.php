<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Init extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('discounts', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedSmallInteger('value');
        });

        \Schema::create('vouchers', function(Blueprint $table){
            $table->increments('id');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->unsignedInteger('discount_id');

            $table->foreign('discount_id')
                ->references('id')
                ->on('discounts')
                ->onDelete('cascade');
        });

        \Schema::create('products', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->decimal('price');
            $table->smallInteger('available')->default(1);
        });

        \Schema::create('product_vouchers', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('voucher_id');


            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('voucher_id')
                ->references('id')
                ->on('vouchers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('product_vouchers');
        \Schema::dropIfExists('products');
        \Schema::dropIfExists('vouchers');
        \Schema::dropIfExists('discounts');
    }
}
