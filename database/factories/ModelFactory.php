<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Discount::class, function(Faker\Generator $faker){
    return [
        'value' => 10
    ];
});

$factory->define(\App\Models\Voucher::class, function(Faker\Generator $faker){
    return [
        'start_date' => $faker->date(),
        'end_date' => $faker->date(),
        'discount_id' => function(){
            return factory(App\Models\Discount::class)->create()->id;
        }
    ];
});

$factory->define(\App\Models\Product::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->name,
        'price' => $faker->numberBetween(100, 10000)
    ];
});

$factory->state(\App\Models\Voucher::class, 'active', function (Faker\Generator $faker){
    return [
        'start_date' => '2017-01-01',
        'end_date' => '2017-12-31'
    ];
});

$factory->state(\App\Models\Voucher::class, 'non-active', function (Faker\Generator $faker){
    return [
        'start_date' => '2018-01-01',
        'end_date' => '2018-12-31'
    ];
});