<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('voucher', [
    'uses' => 'VoucherController@store',
    'as' => 'api.voucher.store'
]);

Route::post('product', [
    'uses' => 'ProductController@store',
    'as' => 'api.product.store'
]);
Route::post('product/buy/{product}', [
    'uses' => 'ProductController@buy',
    'as' => 'api.product.buy'
]);

Route::post('product/{product}/vouchers/{voucher}', [
    'uses' => 'ProductVoucherController@store',
    'as' => 'api.product.vouchers.store'
]);
Route::delete('product/{product}/vouchers/{voucher}', [
    'uses' => 'ProductVoucherController@remove',
    'as' => 'api.product.vouchers.remove'
]);